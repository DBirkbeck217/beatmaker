
function play(){
    
    //Web Audio API

    let audioContext;
    
    try {
      audioContext =
        new (window.AudioContext || window.webkitAudioContext)();
    } catch (error) {
      window.alert(
        `Sorry, but your browser doesn't support the Web Audio API!`
      );
    }
    
    if (audioContext !== undefined) {
        const oscillator = audioContext.createOscillator();
        oscillator.connect(audioContext.destination);
        oscillator.start(audioContext.currentTime );
        oscillator.stop(audioContext.currentTime + .5);
        console.log("sound");
    }
}


//button container

let viewport=document.createElement('div');
viewport.className="viewport";
document.body.appendChild(viewport);

//buttons
for(i=0;i<3;i++){
    let row=document.createElement('div');
    row.className="row";
    viewport.appendChild(row);
    for(j=0;j<4;j++){
        let button=document.createElement('button');
        button.className="buttons";
        button.id="audio"+i+j;
        button.onclick=function play1(){
            play();
        };
        row.appendChild(button);
    }
}



